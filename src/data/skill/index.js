import { DiAws, DiDocker, DiGit, DiJava, DiJavascript1, DiLinux, DiMongodb, DiNodejs, DiPython, DiReact, DiSwift } from "react-icons/di";
import { SiAndroidauto, SiFirebase, SiGithubactions, SiKotlin, SiMacos, SiNextdotjs, SiPostgresql, SiPostman, SiRedis, SiRender, SiVercel, SiVsco, SiXcode } from "react-icons/si";
import { TbBrandReactNative } from "react-icons/tb";

export default {
    skillSet :   [
        
          {
            icon:<TbBrandReactNative/>
          },
          {
            icon:<DiSwift/>
          },
          {
            icon : <DiJava />
          },
          {
            icon:<SiKotlin/>
          },
        {
          icon :  <DiJavascript1 />
        },
        {
          icon :  <DiNodejs />
        },
        
        {
          icon :  <DiMongodb />
        },
        {
          icon : <SiNextdotjs /> 
        },
        {
          icon : <DiGit />
        },
        {
          icon :   <SiFirebase /> 
        },
        {
          icon :  <SiRedis />
        },
        {
          icon : <SiPostgresql />
        },
        {
          icon :  <DiPython />
        },
       
      ],

      toolSet:[
        {
            icon:<DiLinux/>
        },
        {
            icon:<SiMacos />
        },
        {
            icon:<SiXcode/>
        },
        {
            icon:<SiAndroidauto/>
        },
        {
            icon:<SiVsco/>
        },
        {
            icon:<SiPostman/>
        },
        {
            icon:<SiRender/>
        },
        {
            icon:<SiVercel/>
        },
        {
            icon:<DiDocker/>
        },
        {
            icon:<SiGithubactions/>
        },
        {
            icon:<DiAws/>
        }
      ]
}